package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Paint;

/**
 * A decorator for specifying the stroke (foreground) color for drawing the
 * shape.
 */
public class Stroke implements Shape {

	// TODO entirely your job
	protected final Shape shape;

	public Stroke(final int color, final Shape shape) {

		this.shape = shape;

	}

	public int getColor() {
		return 0; //unsure here
	}

	public Shape getShape() {
		return shape;
	}

	@Override
	public <Result> Result accept(Visitor<Result> v) {
		return v.onStroke(this);
	}
}
