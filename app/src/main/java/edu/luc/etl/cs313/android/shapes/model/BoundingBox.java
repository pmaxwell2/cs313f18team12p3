package edu.luc.etl.cs313.android.shapes.model;
import java.util.*;
/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
}

	@Override
	public Location onFill(final Fill f) {
		final Shape fillShape = f.getShape();
		return new Location(0,0, fillShape);
	}



	@Override
	public Location onGroup(final Group g) {
		//
		return null;
	}

	@Override
	public Location onLocation(final Location l) {
		final int x = 1;
		final int y = 1;
		return new Location(x, y,null);

	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int Height = r.getHeight();
		final int Width = r.getWidth();
		return new Location(Height,Width, r);
	}



	@Override
	public Location onStroke(final Stroke c) {
		final Shape strokeShape = c.getShape();
		return new Location(0,0, strokeShape);
	}

	@Override
	public Location onOutline(final Outline o) {
		final Shape outlineShape = o.getShape();
		return new Location(0,0,outlineShape);
	}

	@Override
	public Location onPolygon(final Polygon s)
	{
		//final List<Integer> Points = s.getPoints();
		//return new Location(0, 0, Points);
		return null;
	}
}
